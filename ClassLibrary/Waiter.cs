﻿using System;
using System.Threading;

namespace ClassLibrary
{
    public static class Waiter
    {
        public static void Wait( int seconds )
        {
            for( int i = seconds; i > 0; --i )
            {
                Console.WriteLine( "Waiting for " + i + " seconds" );
                Thread.Sleep( 1000 );
            }

            Console.WriteLine( "Done waiting!" );
        }
    }
}
