﻿using System;
using ClassLibrary;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Waiter.Wait( 5 );

            Console.WriteLine();
            Console.WriteLine( "Press any key to continue..." );
            Console.ReadKey();
        }
    }
}
